const express = require('express');
const cors = require('cors');
const path = require('path');
const fs = require('fs');
const engine = require('mustache-express');

const app = express();
app.use(cors({
    origin: '*',
    allowedHeaders: '*',
}));

app.use('/data', express.static(path.join(__dirname, '..', 'data')));

app.set('views', path.join(__dirname, '..'));
app.engine('html', engine());
app.set('view engine', 'html');

app.get('/scene', (req, res) => {
    return res.render(path.join(__dirname, '..', `index-local.html`), {
        token: req.query.token
    })
});

app.get('/scene/:hash', (req, res) => {
    if (fs.existsSync(path.join(__dirname, '..', req.params.hash))) {
        return res.sendFile(path.join(__dirname, '..', req.params.hash));
    }
    return res.render(path.join(__dirname, '..', `index-local.html`), {
        token: req.query.token,
        hash: req.params.hash,
    })
});

app.get('/scene/*', (req, res) => {
    let url = req.url.replace(/^\/scene/, '').replace(/\/$/, '');
    if (fs.existsSync(path.join(__dirname, '..', url))) {
        return res.sendFile(path.join(__dirname, '..', url));
    }
    url = url.replace(/^\/?[^\/]*/, '');
    if (fs.existsSync(path.join(__dirname, '..', url))) {
        return res.sendFile(path.join(__dirname, '..', url));
    }
    return res.sendStatus(404);
});

app.get('/*', (req, res) => {
    if (fs.existsSync(path.join(__dirname, '..', req.url))) {
        return res.sendFile(path.join(__dirname, '..', req.url));
    }
    return res.sendStatus(404);
});

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Listening on :${port}`));
