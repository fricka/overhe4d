import {FilesInputStore, SceneSerializer, ShaderMaterial, Tools as BabylonTools} from 'babylonjs';

import ProjectExporter from '../project/project-exporter';

import Tools from '../tools/tools';
import Editor from '../editor';
import {SceneUploadResult} from "../../types/SceneUploadResult";
import {Constants} from "../../types/Constants";
import {
    Constants as Cnst,
    Geometry,
    Light,
    Material,
    Mesh,
    Scene,
    SceneComponentConstants,
    SerializationHelper,
    Tags
} from "babylonjs";

let serializeMeshInternal = function serialize(mesh: Mesh, serializationObject: any): void {
    serializationObject.name = mesh.name;
    serializationObject.id = mesh.id;
    serializationObject.type = mesh.getClassName();

    if (Tags && Tags.HasTags(mesh)) {
        serializationObject.tags = Tags.GetTags(mesh);
    }

    serializationObject.position = mesh.position.asArray();

    if (mesh.rotationQuaternion) {
        serializationObject.rotationQuaternion = mesh.rotationQuaternion.asArray();
    } else if (mesh.rotation) {
        serializationObject.rotation = mesh.rotation.asArray();
    }

    serializationObject.scaling = mesh.scaling.asArray();
    if ((mesh as any)._postMultiplyPivotMatrix) {
        serializationObject.pivotMatrix = mesh.getPivotMatrix().asArray();
    } else {
        serializationObject.localMatrix = mesh.getPivotMatrix().asArray();
    }

    serializationObject.isEnabled = mesh.isEnabled(false);
    serializationObject.isVisible = mesh.isVisible;
    serializationObject.infiniteDistance = mesh.infiniteDistance;
    serializationObject.pickable = mesh.isPickable;

    serializationObject.receiveShadows = mesh.receiveShadows;

    serializationObject.billboardMode = mesh.billboardMode;
    serializationObject.visibility = mesh.visibility;

    serializationObject.checkCollisions = mesh.checkCollisions;
    serializationObject.isBlocker = mesh.isBlocker;
    serializationObject.overrideMaterialSideOrientation = mesh.overrideMaterialSideOrientation;

// Parent
    if (mesh.parent) {
        serializationObject.parentId = mesh.parent.id;
    }

// Geometry
    serializationObject.isUnIndexed = mesh.isUnIndexed;
    let geometry = mesh._geometry;
    if (geometry) {
        let geometryId = geometry.id;
        serializationObject.geometryId = geometryId;

        // SubMeshes
        serializationObject.subMeshes = [];
        for (let subIndex = 0; subIndex < (mesh.subMeshes || []).length; subIndex++) {
            let subMesh = mesh.subMeshes[subIndex];

            serializationObject.subMeshes.push({
                materialIndex: subMesh.materialIndex,
                verticesStart: subMesh.verticesStart,
                verticesCount: subMesh.verticesCount,
                indexStart: subMesh.indexStart,
                indexCount: subMesh.indexCount
            });
        }
    }

// Material
    if (mesh.material) {
        if (!mesh.material.doNotSerialize) {
            serializationObject.materialId = mesh.material.id;
        }
    } else {
        mesh.material = null;
    }

// Morph targets
    if (mesh.morphTargetManager) {
        serializationObject.morphTargetManagerId = mesh.morphTargetManager.uniqueId;
    }

// Skeleton
    if (mesh.skeleton) {
        serializationObject.skeletonId = mesh.skeleton.id;
    }

// Physics
//TODO implement correct serialization for physics impostors.
    if (mesh.getScene()._getComponent(SceneComponentConstants.NAME_PHYSICSENGINE)) {
        let impostor = mesh.getPhysicsImpostor();
        if (impostor) {
            serializationObject.physicsMass = impostor.getParam("mass");
            serializationObject.physicsFriction = impostor.getParam("friction");
            serializationObject.physicsRestitution = impostor.getParam("mass");
            serializationObject.physicsImpostor = impostor.type;
        }
    }

// Metadata
    if (mesh.metadata) {
        serializationObject.metadata = mesh.metadata;
    }

// Instances
    serializationObject.instances = [];
    for (let index = 0; index < mesh.instances.length; index++) {
        let instance = mesh.instances[index];
        if (instance.doNotSerialize) {
            continue;
        }

        let serializationInstance: any = {
            name: instance.name,
            id: instance.id,
            position: instance.position.asArray(),
            scaling: instance.scaling.asArray()
        };

        if (instance.parent) {
            serializationInstance.parentId = instance.parent.id;
        }

        if (instance.rotationQuaternion) {
            serializationInstance.rotationQuaternion = instance.rotationQuaternion.asArray();
        } else if (instance.rotation) {
            serializationInstance.rotation = instance.rotation.asArray();
        }
        serializationObject.instances.push(serializationInstance);

        // Animations
        SerializationHelper.AppendSerializedAnimations(instance, serializationInstance);
        serializationInstance.ranges = instance.serializeAnimationRanges();
    }

//

// Animations
    SerializationHelper.AppendSerializedAnimations(mesh, serializationObject);
    serializationObject.ranges = mesh.serializeAnimationRanges();

// Layer mask
    serializationObject.layerMask = mesh.layerMask;

// Alpha
    serializationObject.alphaIndex = mesh.alphaIndex;
    serializationObject.hasVertexAlpha = mesh.hasVertexAlpha;

// Overlay
    serializationObject.overlayAlpha = mesh.overlayAlpha;
    serializationObject.overlayColor = mesh.overlayColor.asArray();
    serializationObject.renderOverlay = mesh.renderOverlay;

// Fog
    serializationObject.applyFog = mesh.applyFog;

// Action Manager
    if (mesh.actionManager) {
        serializationObject.actions = mesh.actionManager.serialize(mesh.name);
    }
};

let serializedGeometries: Geometry[] = [];
let serializeGeometry = (geometry: Geometry, serializationGeometries: any): any => {
    if ((serializedGeometries as any)[geometry.id]) {
        return;
    }

    if (geometry.doNotSerialize) {
        return;
    }

    serializationGeometries.vertexData.push(geometry.serializeVerticeData());

    (serializedGeometries as any)[geometry.id] = true;
};

let serializeMesh = (mesh: Mesh, serializationScene: any): any => {
    let serializationObject: any = {};

    // Geometry
    let geometry = mesh._geometry;
    if (geometry) {
        if (!mesh.getScene().getGeometryByID(geometry.id)) {
            // Geometry was in the memory but not added to the scene, nevertheless it's better to serialize to be able to reload the mesh with its geometry
            serializeGeometry(geometry, serializationScene.geometries);
        }
    }

    // Custom
    if (mesh.serialize) {
        serializeMeshInternal(mesh, serializationObject);
    }

    return serializationObject;
};

export default class SceneExporter {
    // Public members
    public static ProjectExportFormat: 'babylon' | 'glb' | 'gltf' = 'babylon';

    // Private members
    private static _LastRandomId: string = BabylonTools.RandomId();

    /**
     * Creates a new file
     * @param editor: the editor instance
     */
    public static CreateFiles(editor: Editor, format: 'babylon' | 'glb' | 'gltf' = 'babylon'): void {
        // Delete old files
        editor.sceneFile && delete FilesInputStore.FilesToLoad[editor.sceneFile.name];
        editor.projectFile && delete FilesInputStore.FilesToLoad[editor.projectFile.name];

        this._LastRandomId = BabylonTools.RandomId();

        // Do not export shader materials
        editor.core.scene.materials.forEach(m => m instanceof ShaderMaterial && (m.doNotSerialize = true));

        // Serialize
        editor.assets.prefabs.setSerializable(true);
        if (editor.core.scene.soundTracks && editor.core.scene.soundTracks.length === 0)
            editor.core.scene.soundTracks.push(editor.core.scene.mainSoundTrack);
        const serializedScene = this.Serialize(editor.core.scene as any);
        if (editor.core.scene.soundTracks && editor.core.scene.soundTracks.length === 1)
            editor.core.scene.soundTracks.pop();
        editor.assets.prefabs.setSerializable(false);

        if (editor.playCamera)
            serializedScene.activeCameraID = editor.playCamera.id;

        // Metadatas
        this._ClearMetadatas(serializedScene.meshes);
        this._ClearMetadatas(serializedScene.lights);
        this._ClearMetadatas(serializedScene.cameras);
        this._ClearMetadatas(serializedScene.transformNodes);
        this._ClearMetadatas(serializedScene.particleSystems);
        this._ClearMetadatas(serializedScene.materials);
        this._ClearMetadatas(serializedScene.multiMaterials);
        this._ClearMetadatas(serializedScene.skeletons);
        this._ClearMetadatas(serializedScene.sounds);
        this._ClearMetadatas([serializedScene]);

        // Scene "File"
        if (format === 'babylon') {
            editor.sceneFile = Tools.CreateFile(Tools.ConvertStringToUInt8Array(JSON.stringify(serializedScene)), 'scene' + this._LastRandomId + '.babylon');
            FilesInputStore.FilesToLoad[editor.sceneFile.name] = editor.sceneFile;
        }

        // Gui
        editor.guiFiles = [];
        editor.core.uiTextures.forEach(ut => {
            const serializedGui = ut.serialize();
            editor.guiFiles.push(Tools.CreateFile(Tools.ConvertStringToUInt8Array(JSON.stringify(serializedGui)), ut.name + '.gui'));
        });

        // Project
        const name = 'scene' + this._LastRandomId + '.editorproject';
        const project = ProjectExporter.Export(editor);
        editor.projectFile = Tools.CreateFile(Tools.ConvertStringToUInt8Array(JSON.stringify(project)), name);
        FilesInputStore.FilesToLoad[editor.projectFile.name] = editor.projectFile;
    }

    private static Serialize(scene: Scene): any {
        let serializationObject: any = {};

        SceneSerializer.ClearCache();

        // Scene
        serializationObject.useDelayedTextureLoading = scene.useDelayedTextureLoading;
        serializationObject.autoClear = scene.autoClear;
        serializationObject.clearColor = scene.clearColor.asArray();
        serializationObject.ambientColor = scene.ambientColor.asArray();
        serializationObject.gravity = scene.gravity.asArray();
        serializationObject.collisionsEnabled = scene.collisionsEnabled;

        // Fog
        if (scene.fogMode && scene.fogMode !== 0) {
            serializationObject.fogMode = scene.fogMode;
            serializationObject.fogColor = scene.fogColor.asArray();
            serializationObject.fogStart = scene.fogStart;
            serializationObject.fogEnd = scene.fogEnd;
            serializationObject.fogDensity = scene.fogDensity;
        }

        //Physics
        if (scene.isPhysicsEnabled()) {
            let physicEngine = scene.getPhysicsEngine();

            if (physicEngine) {
                serializationObject.physicsEnabled = true;
                serializationObject.physicsGravity = physicEngine.gravity.asArray();
                serializationObject.physicsEngine = physicEngine.getPhysicsPluginName();
            }
        }

        // Metadata
        if (scene.metadata) {
            serializationObject.metadata = scene.metadata;
        }

        // Morph targets
        serializationObject.morphTargetManagers = [];
        for (let abstractMesh of scene.meshes) {
            let manager = (abstractMesh as Mesh).morphTargetManager;

            if (manager) {
                serializationObject.morphTargetManagers.push(manager.serialize());
            }
        }

        // Lights
        serializationObject.lights = [];
        let index: number;
        let light: Light;
        for (index = 0; index < scene.lights.length; index++) {
            light = scene.lights[index];

            if (!light.doNotSerialize) {
                serializationObject.lights.push(light.serialize());
            }
        }

        // Cameras
        serializationObject.cameras = [];
        for (index = 0; index < scene.cameras.length; index++) {
            let camera = scene.cameras[index];

            if (!camera.doNotSerialize) {
                serializationObject.cameras.push(camera.serialize());
            }
        }

        if (scene.activeCamera) {
            serializationObject.activeCameraID = scene.activeCamera.id;
        }

        // Animations
        SerializationHelper.AppendSerializedAnimations(scene, serializationObject);

        // Animation Groups
        if (scene.animationGroups && scene.animationGroups.length > 0) {
            serializationObject.animationGroups = [];
            for (let animationGroupIndex = 0; animationGroupIndex < scene.animationGroups.length; animationGroupIndex++) {
                let animationGroup = scene.animationGroups[animationGroupIndex];

                serializationObject.animationGroups.push(animationGroup.serialize());
            }
        }

        // Reflection probes
        if (scene.reflectionProbes && scene.reflectionProbes.length > 0) {
            serializationObject.reflectionProbes = [];

            for (index = 0; index < scene.reflectionProbes.length; index++) {
                let reflectionProbe = scene.reflectionProbes[index];
                serializationObject.reflectionProbes.push(reflectionProbe.serialize());
            }
        }

        // Materials
        serializationObject.materials = [];
        serializationObject.multiMaterials = [];
        let material: Material;
        for (index = 0; index < scene.materials.length; index++) {
            material = scene.materials[index];
            if (!material.doNotSerialize) {
                serializationObject.materials.push(material.serialize());
            }
        }

        // MultiMaterials
        serializationObject.multiMaterials = [];
        for (index = 0; index < scene.multiMaterials.length; index++) {
            let multiMaterial = scene.multiMaterials[index];
            serializationObject.multiMaterials.push(multiMaterial.serialize());
        }

        // Environment texture
        if (scene.environmentTexture) {
            serializationObject.environmentTexture = scene.environmentTexture.name;
        }

        // Environment Intensity
        serializationObject.environmentIntensity = scene.environmentIntensity;

        // Skeletons
        serializationObject.skeletons = [];
        for (index = 0; index < scene.skeletons.length; index++) {
            let skeleton = scene.skeletons[index];
            if (!skeleton.doNotSerialize) {
                serializationObject.skeletons.push(skeleton.serialize());
            }
        }

        // Transform nodes
        serializationObject.transformNodes = [];
        for (index = 0; index < scene.transformNodes.length; index++) {
            if (!scene.transformNodes[index].doNotSerialize) {
                serializationObject.transformNodes.push(scene.transformNodes[index].serialize());
            }
        }

        // Geometries
        serializationObject.geometries = {};

        serializationObject.geometries.boxes = [];
        serializationObject.geometries.spheres = [];
        serializationObject.geometries.cylinders = [];
        serializationObject.geometries.toruses = [];
        serializationObject.geometries.grounds = [];
        serializationObject.geometries.planes = [];
        serializationObject.geometries.torusKnots = [];
        serializationObject.geometries.vertexData = [];

        serializedGeometries = [];
        let geometries = scene.getGeometries();
        for (index = 0; index < geometries.length; index++) {
            let geometry = geometries[index];

            if (geometry.isReady()) {
                serializeGeometry(geometry, serializationObject.geometries);
            }
        }

        // Meshes
        serializationObject.meshes = [];
        for (index = 0; index < scene.meshes.length; index++) {
            let abstractMesh = scene.meshes[index];

            if (abstractMesh instanceof Mesh) {
                let mesh = abstractMesh;
                if (!mesh.doNotSerialize) {
                    if (mesh.delayLoadState === Cnst.DELAYLOADSTATE_LOADED || mesh.delayLoadState === Cnst.DELAYLOADSTATE_NONE) {
                        serializationObject.meshes.push(serializeMesh(mesh, serializationObject));
                    }
                }
            }
        }

        // Particles Systems
        serializationObject.particleSystems = [];
        for (index = 0; index < scene.particleSystems.length; index++) {
            serializationObject.particleSystems.push(scene.particleSystems[index].serialize());
        }

        // Action Manager
        if (scene.actionManager) {
            serializationObject.actions = scene.actionManager.serialize("scene");
        }

        // Components
        for (let component of scene._serializableComponents) {
            component.serialize(serializationObject);
        }

        return serializationObject;
    }

    /**
     * Creates the babylon scene and a download link for the babylon file
     * @param editor the editor reference
     */
    public static DownloadBabylonFile(editor: Editor): void {
        this.CreateFiles(editor);
        BabylonTools.Download(editor.sceneFile, editor.sceneFile.name);
    }

    private static async CreateImageFromCanvas(editor: Editor): Promise<File> {
        const dataURI = await BabylonTools.CreateScreenshotUsingRenderTargetAsync(editor.core.engine, editor.camera, 512);
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        const byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        //Old Code
        //write the ArrayBuffer to a blob, and you're done
        //var bb = new BlobBuilder();
        //bb.append(ab);
        //return bb.getBlob(mimeString);

        //New Code
        return new File([ab], 'preview.png', {type: mimeString});
    }

    public static async SaveScene(editor: Editor) {
        editor.layout.lockPanel('main', 'Saving scene...', true);
        this.CreateFiles(editor);
        const fd = new FormData();
        const file = new File([editor.sceneFile], 'scene.babylon', {type: 'application/json'});
        const preview = await this.CreateImageFromCanvas(editor);
        [file, preview].forEach(f => fd.append('file[]', f));
        const uploaded: SceneUploadResult = await (await fetch(`${Constants.API}/scene/${Constants.HASH}/upload${Constants.TOKEN != '' ? `?api_token=${Constants.TOKEN}` : ''}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: fd,
        })).json();
        editor.layout.unlockPanel('main');
    }

    // Clears the metadatas from the editor and replace by custom metadatas
    // if exists
    private static _ClearMetadatas(objects: any[]): void {
        if (!objects)
            return;

        // For each object, replace by custom metadata if exists
        objects.forEach(m => {
            if (!m)
                return;

            this._ClearMetadata(m);

            // Second level (typically for textures)
            for (const k in m) {
                const v = m[k];
                if (!v)
                    continue;

                this._ClearMetadata(v);
            }
        });
    }

    // Clears the metadata object
    private static _ClearMetadata(obj: any): void {
        if (obj.metadata) {
            // Clear original saved object
            delete obj.metadata.original;

            if (obj.metadata.baseConfiguration)
                obj.pickable = obj.metadata.baseConfiguration.isPickable;

            if (obj.metadata.customMetadatas)
                obj.metadata = obj.metadata.customMetadatas;
        } else {
            delete obj.metadata;
        }
    }
}
