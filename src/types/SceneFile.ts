export interface SceneFile {
    name: string;
    type: string;
    directory: string;
    absolutePath: string;
    userId: number;
    id: number;
}
