import {SceneFile} from "./SceneFile";

export interface SceneUploadResult {
    files: SceneFile[];
}
