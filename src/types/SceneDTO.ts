import {SceneFile} from "./SceneFile";

export interface SceneDTO {
    id: number;
    identifier: string;
    user_id: number;
    name: string;
    deleted_at?: string;
    created_at?: string;
    updated_at?: string;
    files: SceneFile[];
    edit: boolean;
}
