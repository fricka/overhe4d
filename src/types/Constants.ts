export class Constants {
    public static readonly HASH: string = (window as any).hash;
    public static readonly TOKEN: string = (window as any).token;
    private static readonly BACKEND_URL_BASE: string = 'http://overhe4d.zuffik.eu';
    public static readonly STORAGE = Constants.BACKEND_URL_BASE;
    public static readonly API: string = `${Constants.BACKEND_URL_BASE}/api`;
}
